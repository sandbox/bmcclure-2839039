**SUMMARY**

Allows rendering an entity reference field as a view.

**REQUIREMENTS**

* Views (in Core)


**INSTALLATION**

Install as usual, see http://drupal.org/node/1897420 for further information.


**CONTACT**

Current maintainers:
* [bmcclure](https://www.drupal.org/user/278485)
